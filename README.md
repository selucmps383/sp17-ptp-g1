
#Xamarin Studio#
>Use the Visual Studio with Xamarin Studio integration to develop cross-platform apps for:
 * iOS
 * Android
 * OSX
 * Linux
 * Windows
>Also supports integration with xCode

##Code Sharing##
> * Use Xamarin to create native apps for different platforms all using C#.
>> * Xamarin Studio also gives the ability to call existing Objective-C code for iOS and Java packages and code for Android development

##Xamarin.forms vs. Xamarin.iOS and Xamarin.Android##

> * Xamarin forms gives developers options to use Code Sharing to the fullest with many functions and templates that make writing mobile apps easy and efficient.
> * If a developer would like to create similar apps with different features dependent on platform, Xamarin also offers projects for only iOS or Android.

###Shared Projects
> * For times when developers would like to access packages for a specific platform/ incorporate things such as database connectivity, Xamarin Studio also offers what are called "#if directives".
> * These #if directives check the platform at the time of compilation and only compiles source code that is needed for the specific platform it's being executed on.

>![text] (https://developer.xamarin.com/guides/cross-platform/application_fundamentals/building_cross_platform_applications/sharing_code_options/Images/SharedAssetProject.png "example of Shared Assets")

###PCL/Portable Class Libraries###
> * As an alternative to a shared project, PCLs aim to collect only packages that are being used by an application.
> * This makes testing and bug fixing much easier since the developer is able to hand select which packages are being used by each platform. There is also the benefit of not having to include #if directives, making the app more portable.
> 
> ![text] (https://developer.xamarin.com/guides/cross-platform/application_fundamentals/building_cross_platform_applications/sharing_code_options/Images/PortableClassLibrary.png "example of PCLs")

###.NET Standard###
> * the .NETstandard library allows for all platforms in .NET to use the same base class library. This allows for developers to learn one library to create multi-platform apps. 
> 
> ![text] (https://msdnshared.blob.core.windows.net/media/2016/09/dotnet-tomorrow.png ".NET standard library")

> * The .NET standard library also keeps applications consistent and allows for components to be split up into Nuget packages.

###Components vs. Nuget Packages###
> * Xamarin components and Nuget packages are very similar. Often, components contain Nuget packages. Components, however, also contain sample code and documentation where Nugets do not. 

> * Essentially, the ability to add references manually as a Nuget package alone gives developers more freedom with how dependencies are used while still allowing components to be added to a shared project.


####Sources####
> * https://developer.xamarin.com
> * https://blogs.msdn.microsoft.com/dotnet/2016/09/26/introducing-net-standard/
> 
#####Blake Weimer#####
